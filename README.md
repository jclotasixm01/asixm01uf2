# asixm01uf2
## ASIX M01 UF2 Curs 2019-2020


 * [Gparted]( https://gparted.org) 
 * [Ubuntu](https://ubuntu.com/#download)
 * W10 Màquina virtual que es passarà a classe

### Activitats

#### NF1-Gestió de la informació

Activitat A1 - El sistema d'arxius i l'estructura de directoris

 * [Activitats-NF1-A1.pdf](https://gitlab.com/jclotdawm01/dawm01uf2/blob/master/Activitats-NF1-A1.pdf) 
   * [Material IOC Administració de la informació](https://gitlab.com/jclotdawm01/dawm01uf2/blob/master/fp_asix_m01_u6_pdfindex.pdf)
 * [EAC1](https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/EAC1_asixm01uf2a1p1.pdf)


Activitat A2 - Gestió de la informació (rendiment i recursos)

 * [Activitats-NF1-A2.pdf](https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/Activitats-NF1-A2.pdf) 
   * [Material IOC Administració de la informació](https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/fp_asix_m01_u6_pdfindex.pdf)
   * [Material IOC Seguretat, rendiment i recursos](https://https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/fp_asix_m01_u9_pdfindex.pdf)
 * [EAC2](https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/EAC2_asixm01uf2a1p2.pdf)

Activitat A3 - Administració de discos

 * [Activitats-NF1-A3.pdf](https://gitlab.com/jclotdawm01/asixm01uf2/blob/master/Activitats-NF1-A3.pdf)
   * [Material IOC Administració de la informació](https://gitlab.com/jclotasixm01/asixm01uf2/blob/master/fp_asix_m01_u6_pdfindex.pdf)


